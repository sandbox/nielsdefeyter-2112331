<?php

/**
 * @file
 * Field Declarations
 */

/**
  * Called directly from mirrors_commerce.module
  * User hook_field_types_alter() from other modules
 */
function mirrors_commerce_field_types_product(&$field_types) {
  $field_types['product'] = array(
    'views' => array(),
    'feeds' => array(),
  );
}
