<?php

/**
 * @file
 * Defaults
 */

/**
 * Returns commerce enitity types.
 */
function mirrors_commerce_entity_types_defaults() {
  $entity_types = array();

  // Commerce Entity Types
  $commerce_entity_types = array(
    'product',
    'customer_profile',
    'order',
    'line_item',
    'payment',
  );

  // We do this loop, so our module files are easier to read
  foreach ($commerce_entity_types as $type) {
    module_load_include('inc', 'mirrors_commerce', 'entity/' . $type);

    $function = 'mirrors_commerce_entity_types_' . $type;
    if (function_exists($function)) {
      $function($entity_types);
    }
  }

  return $entity_types;
}

/**
 * Returns commerce field types.
 */
function mirrors_commerce_field_types_defaults() {
  $field_types = array();

  // Commerce Field Types
  $commerce_field_types = array(
    'addressfield',
    'price',
    'product',
  );

  // We do this loop, so our module files are easier to read
  foreach ($commerce_field_types as $type) {
    module_load_include('inc', 'mirrors_commerce', 'field/' . $type);

    $function = 'mirrors_commerce_field_types_' . $type;
    if (function_exists($function)) {
      $function($field_types);
    }
  }

  return $field_types;
}
